package io;

/*
 * Training 1: Hello, World!
 *
 * First, have the program simply output "Hello, World!". (This is standard
 * practice when learning a programming language.)
 *
 * After that, create a new Scanner object, and get the person's name from the
 * standard input stream. Have the program greet you by your name. Show it who's
 * boss.
 */
public class HelloWorld {

	public static void main(String[] args) {

	}

}

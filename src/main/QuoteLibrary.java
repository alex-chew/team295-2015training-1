package main;

/*
 * Training 1: Quote Library
 *
 * This program should use a file to store quotes. When the program starts up,
 * it creates the data file if it does not exist. Then, the program reads the
 * file and prints all quotes onto the screen (with their authors).
 *
 * Quotes are input using the standard input stream, and should contain both the
 * quote text and the quote's author.
 *
 * Use an array to store these two members of each quote.
 */
public class QuoteLibrary {

	public static void main(String[] args) {

	}

}
